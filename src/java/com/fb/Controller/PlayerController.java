package com.fb.Controller;

import com.fb.DTO.PlayerDto;
import com.fb.entity.Player;
import com.fb.service.PlayerService;
import com.fb.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

/**
 * Created by sm on 7/5/17.
 */
@Controller
@RequestMapping("/players")
public class PlayerController {
    @Autowired
    PlayerService playerService;
    @Autowired
    TeamService teamService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String allPlayer(ModelMap modelMap, Principal principal) {
        modelMap.put("username", principal.getName());
        List<PlayerDto> playerDtos = playerService.findAllPlayer();
        modelMap.put("listplayer",playerDtos);
        return "allPlayer";
    }

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String formNewPlayer(ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());

        modelMap.addAttribute("playerDto",new PlayerDto());
        modelMap.put("teams",teamService.findAllTeam());
        return "formNewPlayer";
    }

    @RequestMapping(value = "/add-player", method = RequestMethod.POST)
    public String addPlayer(@ModelAttribute(value = "playerDto") PlayerDto playerDto){
        playerService.addPlayer(playerDto);
        return "redirect:/players";
    }

    @RequestMapping(value = "/edit/{id}" , method = RequestMethod.GET)
    public String FormEditPlayer(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());
        PlayerDto playerDto= playerService.findPlayerById(id);
        modelMap.addAttribute("editplayer",playerDto);
        modelMap.put("teams",teamService.findAllTeam());
        return "formEditPlayer";
    }

    @RequestMapping(value = "/edit-player", method = RequestMethod.POST)
    public String editPlayer(@ModelAttribute(value = "editplayer") PlayerDto playerDto){
        playerService.updatePlayer(playerDto);
        return "redirect:/players";
    }

    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
    public String deletePlayer(@PathVariable(value = "id") int id){
        playerService.deletePlayerById(id);
        return "redirect:/players";

    }
}
