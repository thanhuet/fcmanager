package com.fb.Controller;

import com.fb.DTO.PlayerRankDto;
import com.fb.entity.League;
import com.fb.service.LeagueService;
import com.fb.service.PlayerMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.Array;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by sm on 7/1/17.
 */
@Controller
public class MainController {
    @Autowired
    private LeagueService leagueService;
    @Autowired
    PlayerMatchService playerMatchService;

    //////////////////////////////

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String mainDirect(ModelMap modelMap){
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        System.out.println(authorities.size());
        List list = new ArrayList(authorities);
        if(list.get(0).toString().equals("ROLE_ADMIN"))
            return "redirect:/admin";
        else if(list.get(0).toString().equals("ROLE_USER"))
            return "redirect:/user";
        else return  "redirect:/login";
    }


    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(ModelMap modelMap){
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        System.out.println(authorities.size());
        List list = new ArrayList(authorities);
        if(list.get(0).toString().equals("ROLE_ADMIN"))
            return "redirect:/admin";
        else if(list.get(0).toString().equals("ROLE_USER"))
            return "redirect:/user";
        else return  "login";
    }


    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap modelMap, Principal principal) {
        modelMap.put("username",principal.getName());

        List<PlayerRankDto> playerRankDtos = playerMatchService.rank10Player();
        modelMap.put("listrankplayer",playerRankDtos);
        return "adminPage";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userPage(ModelMap modelMap) {
        List<League> list=leagueService.findAllLeague();

        modelMap.put("listleague",list);
        return "userPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(ModelMap modelMap) {
        modelMap.put("title", "Logout");
        return "login";
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public String userInfo(Model model, Principal principal) {


        // Sau khi user login thanh cong se co principal
        String userName = principal.getName();

        System.out.println("User Name: "+ userName);

        return "userInfoPage";
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
            model.addAttribute("message", "Hi " + principal.getName()
                    + "<br> You do not have permission to access this page!");
        } else {
            model.addAttribute("msg",
                    "You do not have permission to access this page!");
        }
        return "page_403";
    }

    @RequestMapping("/login-failure")
    public String loginFailure(){

        return "redirect:/login";
    }

}
