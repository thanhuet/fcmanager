package com.fb.Controller;

import com.fb.entity.Round;
import com.fb.service.MatchService;
import com.fb.service.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by sm on 7/6/17.
 */
@Controller
@RequestMapping("/rounds")
public class Roundcontroller {
    @Autowired
    RoundService roundService;
    @Autowired
    MatchService matchService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public String detailRound(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());
        Round round=roundService.findRoundById(id);
        modelMap.put("roundname",round.getName());
        modelMap.put("roundid",round.getId());
        modelMap.put("listMatch",matchService.convertToDTO(round.getMatches()));
        return "matchesOfRound";
    }
}
