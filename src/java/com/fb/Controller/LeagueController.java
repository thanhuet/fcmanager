package com.fb.Controller;

import com.fb.entity.League;
import com.fb.entity.Season;
import com.fb.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by sm on 7/1/17.
 */
@Controller
@RequestMapping("/leagues")
public class LeagueController {
    @Autowired
    LeagueService leagueService;
    @RequestMapping(value = "",method =  RequestMethod.GET)
    public String allTeams(ModelMap modelMap, Principal principal){
        List<League> list=leagueService.findAllLeague();
        modelMap.put("listleague",list);

        String userName = principal.getName();
        modelMap.put("username",userName);
        return "allLeague";
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public String  detailLeague(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());
        League league=leagueService.findLeagueById(id);
        List<Season> seasons=league.getSeasons();
        for(Season season : seasons ){
            System.out.println(season);
        }
        modelMap.put("listseason",seasons);
        return "seasonsOfLeague";
    }
}
