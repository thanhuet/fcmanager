package com.fb.Controller;

import com.fb.DTO.TeamDto;
import com.fb.entity.Player;
import com.fb.entity.Team;
import com.fb.service.PlayerService;
import com.fb.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

/**
 * Created by sm on 7/4/17.
 */
@Controller
@RequestMapping("/teams")
public class TeamController {
    @Autowired
    TeamService teamService;
    @Autowired
    PlayerService playerService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public String allTeams(ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());
        List<TeamDto> teamDtos= teamService.findAllTeam();
        modelMap.put("listteam",teamDtos);
        return "allTeam";
    }

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String formAddNewTeam(ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());
        modelMap.addAttribute("teamDto",new TeamDto());
        return "formNewTeam";
    }

    @RequestMapping(value = "/add-team",method = RequestMethod.POST)
    public String addTeam(@ModelAttribute("teamDto") TeamDto teamDto , ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());

        teamService.addTeam(teamDto);
        return "redirect:/teams/";
    }

    @RequestMapping(value="/edit/{id}",method = RequestMethod.GET)
    public String formEditTeam(@PathVariable(value = "id") int id,ModelMap modelMap, Principal principal){
        modelMap.put("username",principal.getName());

        TeamDto teamDto= teamService.findTeamById(id);
        modelMap.addAttribute("editteam",teamDto);

        modelMap.put("listplayer",playerService.findPlayersOfTeam(id));
        return "formEditTeam";
    }

    @RequestMapping(value = "/edit-team",method = RequestMethod.POST)
    public  String editTeam(@ModelAttribute("editteam") TeamDto teamDto , ModelMap modelMap, Principal principal){
        System.out.println(teamDto.getId()+"   "+teamDto.getName()+"   "+teamDto.getCaptian()+"  "+teamDto.getCoach());
        teamService.updateTeam(teamDto);
        return "redirect:/teams/";
    }

    @RequestMapping(value="/delete/{id}",method = RequestMethod.GET)
    public String deleteTeam(@PathVariable(value = "id") int id){
        teamService.deleteTeamById(id);
        return "redirect:/teams/";
    }

}
