package com.fb.Controller;


import com.fb.DTO.MatchDto;
import com.fb.DTO.TeamDto;
import com.fb.service.MatchService;
import com.fb.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

/**
 * Created by sm on 7/5/17.
 */

@Controller
@RequestMapping("/matches")
public class MatchController {
    @Autowired
    MatchService matchService;
    @Autowired
    TeamService teamService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String allMatch(ModelMap modelMap, Principal principal) {
        modelMap.put("username", principal.getName());
        List<MatchDto> matchDtos = matchService.findAllMatch();
        modelMap.put("listmatch", matchDtos);
        return "allMatch";
    }

    @RequestMapping(value = "/add/round-{id}", method = RequestMethod.GET)
    public String formNewMatch(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal) {
        modelMap.put("username", principal.getName());

        MatchDto matchDto = new MatchDto();
        matchDto.setRoundId(id);
        modelMap.addAttribute("matchDto", matchDto);
        List<TeamDto> teamDtos = teamService.findAllTeam();
        modelMap.put("roundId", id);
        modelMap.put("teams", teamDtos);
        return "formNewMatch";
    }


    @RequestMapping(value = "/add-match", method = RequestMethod.POST)
    public String addMatch(@ModelAttribute("matchDto") MatchDto matchDto) {
        matchService.addMatch(matchDto);
        return "redirect:/rounds/" + matchDto.getRoundId();
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String formEditMatch(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal) {
        modelMap.put("username", principal.getName());
        MatchDto matchDto = matchService.findMatchById(id);

        modelMap.addAttribute("editmatch",matchDto);
        List<TeamDto> teamDtos = teamService.findAllTeam();
        modelMap.put("teams", teamDtos);
        return "formEditMatch";
    }

    @RequestMapping(value = "/edit-match",method = RequestMethod.POST)
    public String editMatch(@ModelAttribute("matchDto") MatchDto matchDto){
        matchService.updateMatch(matchDto);
        return  "redirect:/matches";
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteMatch(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal) {
        matchService.deleteMatchById(id);
        return "redirect:/matches";
    }


    // not done
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String detailMatch(ModelMap modelMap, Principal principal) {
        modelMap.put("username", principal.getName());
        return null;
    }


}
