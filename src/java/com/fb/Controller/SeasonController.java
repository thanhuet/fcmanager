package com.fb.Controller;

import com.fb.entity.Season;
import com.fb.service.SeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by sm on 7/6/17.
 */
@Controller
@RequestMapping("/seasons")
public class SeasonController {
    @Autowired
    SeasonService seasonService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String detailSeason(@PathVariable(value = "id") int id, ModelMap modelMap, Principal principal) {
        modelMap.put("username",principal.getName());
        Season season = seasonService.findSeasonById(id);
        modelMap.put("season",season);
        return "roundsOfSeason";
    }
}
