package com.fb.Controller;

import com.fb.DTO.PlayerRankDto;
import com.fb.entity.PlayerMatch;
import com.fb.service.PlayerMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

/**
 * Created by sm on 7/9/17.
 */
@Controller
@RequestMapping("/statistic")
public class StatisticController {
    @Autowired
    PlayerMatchService playerMatchService;

    @RequestMapping(value = "/top10-player", method = RequestMethod.GET)
    public String top10Player(ModelMap modelMap, Principal principal) {
        modelMap.put("username",principal.getName());

        List<PlayerRankDto> playerRankDtos = playerMatchService.rank10Player();
        modelMap.put("listrankplayer",playerRankDtos);
        return "redirect:/admin";
    }

    ;
}
