package com.fb.DTO;

import com.fb.entity.Player;

import java.math.BigInteger;

/**
 * Created by sm on 7/9/17.
 */
public class PlayerRankDto {
    BigInteger playerId;
    BigInteger totalGoal;
    Player player;

    public Integer getPlayerId() {
        return playerId.intValue();
    }

    public void setPlayerId(BigInteger playerId) {
        this.playerId = playerId;
    }

    public Integer getTotalGoal() {
        return totalGoal.intValue();
    }

    public void setTotalGoal(BigInteger totalGoal) {
        this.totalGoal = totalGoal;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
