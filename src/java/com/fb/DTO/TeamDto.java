package com.fb.DTO;

/**
 * Created by sm on 7/4/17.
 */
public class TeamDto {
    private int id;
    private String name;
    private String captian;
    private int captian_id;
    private String coach;
    private Long numberPlayer;

    public TeamDto(int id, String name, String captian, int captian_id, String coach) {
        this.id = id;
        this.name = name;
        this.captian = captian;
        this.captian_id = captian_id;
        this.coach = coach;
    }

    public TeamDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaptian() {
        return captian;
    }

    public void setCaptian(String captian) {
        this.captian = captian;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCaptian_id() {
        return captian_id;
    }

    public void setCaptian_id(int captian_id) {
        this.captian_id = captian_id;
    }

    public Long getNumberPlayer() {
        return this.numberPlayer;
    }

    public void setNumberPlayer(Long numberPlayer) {
        this.numberPlayer = numberPlayer;
    }
}
