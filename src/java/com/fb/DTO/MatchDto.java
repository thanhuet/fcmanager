package com.fb.DTO;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by sm on 7/5/17.
 */
public class MatchDto {
    private int id;
    private int homeTeamId;
    private String  homeTeamName;
    private int awayTeamId;
    private String  awayTeamName;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date startTime;
    private int roundId;
    private String roundName;
    private String description;
    private int homeGoal;
    private int awayGoal;

    public MatchDto() {

    }

    public MatchDto(int id, int homeTeamId, String homeTeamName, int awayTeamId, String  awayTeamName, Date startTime, int roundId, String roundName, String description) {
        this.id = id;
        this.homeTeamId = homeTeamId;
        this.homeTeamName = homeTeamName;
        this.awayTeamId = awayTeamId;
        this.awayTeamName = awayTeamName;
        this.startTime = startTime;
        this.roundId = roundId;
        this.roundName = roundName;
        this.description = description;
    }

    public MatchDto(int id, int homeTeamId, String homeTeamName, int awayTeamId, String  awayTeamName, Date startTime, int roundId, String roundName, int homeGoal ,int awayGoal,String description) {
        this.id = id;
        this.homeTeamId = homeTeamId;
        this.homeTeamName = homeTeamName;
        this.awayTeamId = awayTeamId;
        this.awayTeamName = awayTeamName;
        this.startTime = startTime;
        this.roundId = roundId;
        this.roundName = roundName;
        this.homeGoal=homeGoal;
        this.awayGoal=awayGoal;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHomeTeamId() {
        return homeTeamId;
    }

    public void setHomeTeamId(int homeTeamId) {
        this.homeTeamId = homeTeamId;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public int getAwayTeamId() {
        return awayTeamId;
    }

    public void setAwayTeamId(int awayTeamId) {
        this.awayTeamId = awayTeamId;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoundName() {
        return roundName;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    public int getHomeGoal() {
        return homeGoal;
    }

    public void setHomeGoal(int homeGoal) {
        this.homeGoal = homeGoal;
    }

    public int getAwayGoal() {
        return awayGoal;
    }

    public void setAwayGoal(int awayGoal) {
        this.awayGoal = awayGoal;
    }
}
