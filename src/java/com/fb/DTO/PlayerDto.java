package com.fb.DTO;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by sm on 7/5/17.
 */
public class PlayerDto {
    private int id;
    private String name;
    private short number;
    private int age;
    private short height;
    private short weight;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date joined;
    private String position;
    private int team_id;
    private String team_name;

    public PlayerDto() {
    }

    public PlayerDto(String name) {
        this.name = name;
    }

    public PlayerDto(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public PlayerDto(int id, String name,int age, short height, short weight, Date joined,int team_id, String team_name, String position) {
        this.id = id;
        this.name = name;
        this.age= age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.team_id=team_id;
        this.team_name=team_name;
        this.position = position;
    }

    public PlayerDto(int id, String name,int age, short height, short weight, Date joined, String position, int team_id, String team_name) {
        this.id = id;
        this.name = name;
        this.age= age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.position = position;
        this.team_id = team_id;
        this.team_name = team_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public short getHeight() {
        return height;
    }

    public void setHeight(short height) {
        this.height = height;
    }

    public short getWeight() {
        return weight;
    }

    public void setWeight(short weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public short getNumber() {
        return number;
    }

    public void setNumber(short number) {
        this.number = number;
    }
}
