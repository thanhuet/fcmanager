package com.fb.configuration;

import com.fb.service.Impl.MyUserDetailsService;
import com.fb.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sm on 7/2/17.
 */

@Configuration
//@EnableWebSecurity
@EnableWebMvcSecurity
@Transactional
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyUserDetailsService userInfoService;

    @Autowired
    CustomAuthenticationFailureHandler customFailureHandler;

    @Autowired
    CustomAuthenticationSucessHandler customSucessHandler;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

//        auth.userDetailsService(userInfoService);
        auth.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();


        http.authorizeRequests().antMatchers("/", "/welcome", "/login", "/logout","/index").permitAll();

        http.authorizeRequests().antMatchers("/userInfo").access("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')");

        http.authorizeRequests().antMatchers("/admin").access("hasRole('ROLE_ADMIN')");


        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

        http.authorizeRequests().and().formLogin()//

                .loginProcessingUrl("/j_spring_security_check")
                .loginPage("/login")
//                .defaultSuccessUrl("/")
//                .failureUrl("/login-failure")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(customSucessHandler)
                .failureHandler(customFailureHandler)
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/logoutSuccessful");

    }

    @Bean
    public DaoAuthenticationProvider authProvider(){
        DaoAuthenticationProvider authProvider =new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userInfoService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
//        System.out.println(new BCryptPasswordEncoder(11).encode("111"));
        return new BCryptPasswordEncoder(11);
    }


}
