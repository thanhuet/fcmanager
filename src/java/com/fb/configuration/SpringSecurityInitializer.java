package com.fb.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by sm on 7/2/17.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
