package com.fb.util;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.util.ArrayList;
import java.util.List;

public class ModelMapperUtil {

    ModelMapper modelMapper = null;

    public ModelMapperUtil() {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public <objectMap extends Object> objectMap mapper(Object objectData, Class<objectMap> mapClass) {
        return (objectMap) modelMapper.map(objectData, mapClass);
    }

    public <objectMap extends Object> List<objectMap> listMapper(List<Object> objectDatas, Class<objectMap> mapClass) {
            List<objectMap> objectMaps=new ArrayList<>();
            for(Object object: objectDatas){
                objectMaps.add(modelMapper.map(object, mapClass));
            }
        return objectMaps;
    }

//    public List<KeyValueBean> convertListFormToBean(List<? extends BaseCustomDTO> listForm) {
//        List<KeyValueBean> lstBean = new ArrayList<KeyValueBean>();
//        if (listForm != null) {
//            for (DTOInterface form : listForm) {
//                lstBean.add(form.toBean());
//            }
//        }
//        return lstBean;
//    }
}