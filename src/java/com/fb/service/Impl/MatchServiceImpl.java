package com.fb.service.Impl;

import com.fb.DAO.MatchDao;
import com.fb.DAO.PlayerMatchDao;
import com.fb.DAO.RoundDao;
import com.fb.DAO.TeamDao;
import com.fb.DTO.MatchDto;
import com.fb.entity.Match;
import com.fb.entity.Round;
import com.fb.entity.Team;
import com.fb.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 7/5/17.
 */
@Service("MatchService")
@Transactional
public class MatchServiceImpl implements MatchService {
    @Autowired
    MatchDao matchDao;
    @Autowired
    TeamDao teamDao;
    @Autowired
    RoundDao roundDao;
    @Autowired
    PlayerMatchDao playerMatchDao;

    @Override
    public MatchDto findMatchById(Integer id) {
        Match match = matchDao.findMatchById(id);
        return convertToDTO(match);
    }

    @Override
    public void addMatch(MatchDto matchDto) {
        Round round = roundDao.findRoundById(matchDto.getRoundId());
        Team homeTeam = teamDao.findTeamById(matchDto.getHomeTeamId());
        Team awayTeam = teamDao.findTeamById(matchDto.getAwayTeamId());
        Date startTime = matchDto.getStartTime();
        String description = matchDto.getDescription();
        matchDao.addMatch(new Match(homeTeam, awayTeam, startTime, round, description));
    }

    @Override
    public void deleteMatchById(Integer id) {
        playerMatchDao.deleteMatchPlayerByMatch(id);
        //
        matchDao.deleteMatchById(id);
    }

    @Override
    public void updateMatch(MatchDto matchDto) {
        matchDao.updateMatch(convertToModel(matchDto));
    }

    @Override
    public List<MatchDto> findAllMatch() {
        List<Match> matches = matchDao.findAllMatch();
        return convertToDTO(matches);
    }

    public MatchDto convertToDTO(Match match) {
        MatchDto matchDto=new MatchDto();
        matchDto.setId(match.getId());
        matchDto.setHomeTeamId(match.getHomeTeam().getId());
        matchDto.setHomeTeamName(match.getHomeTeam().getName()) ;
        matchDto.setAwayTeamId(match.getAwayTeam().getId()) ;
        matchDto.setAwayTeamName( match.getAwayTeam().getName());
        matchDto.setStartTime(match.getStartTime()) ;
        matchDto.setRoundId(match.getRound().getId()) ;
        matchDto.setRoundName(match.getRound().getName());
        matchDto.setHomeGoal(match.getHomeGoal());
        matchDto.setAwayGoal(match.getAwayGoal());
        matchDto.setDescription(match.getDescription());
        return matchDto;
    }


    public List<MatchDto> convertToDTO(List<Match> matches) {
        List<MatchDto> matchDtos = new ArrayList<>();
        for (Match match : matches) {
            matchDtos.add(convertToDTO(match));
        }
        return matchDtos;
    }

    public Match convertToModel(MatchDto matchDto) {
        Match match = matchDao.findMatchById(matchDto.getId());
        match.setRound(roundDao.findRoundById(matchDto.getRoundId()));
        match.setHomeTeam(teamDao.findTeamById(matchDto.getHomeTeamId()));
        match.setAwayTeam(teamDao.findTeamById(matchDto.getAwayTeamId()));
        match.setStartTime(matchDto.getStartTime());
        match.setDescription(matchDto.getDescription());
        match.setHomeGoal(matchDto.getHomeGoal());
        match.setAwayGoal(matchDto.getAwayGoal());
        return match;
    }
}
