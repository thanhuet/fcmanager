package com.fb.service.Impl;

import com.fb.DAO.SeasonDao;
import com.fb.entity.Season;
import com.fb.service.SeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by sm on 7/6/17.
 */
@Service("SeasonService")
@Transactional
public class SeasonServiceImpl implements SeasonService{
    @Autowired
    SeasonDao seasonDao;

    @Override
    public Season findSeasonById(Integer id) {
        return seasonDao.findSeasonWithRoundById(id);
    }

    @Override
    public void addSeason(Season season) {
        seasonDao.addSeason(season);
    }

    @Override
    public void deleteSeasonById(Integer id) {
        seasonDao.deleteSeasonById(id);
    }

    @Override
    public void updateSeason(Season season) {
        seasonDao.updateSeason(season);
    }

    @Override
    public List<Season> findAllSeason() {
        return seasonDao.findAllSeason();
    }
}
