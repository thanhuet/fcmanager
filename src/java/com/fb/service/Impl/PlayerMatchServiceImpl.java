package com.fb.service.Impl;

import com.fb.DAO.PlayerMatchDao;
import com.fb.DTO.PlayerRankDto;
import com.fb.entity.PlayerMatch;
import com.fb.service.PlayerMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by sm on 7/9/17.
 */
@Service("PlayerMatchService")
@Transactional
public class PlayerMatchServiceImpl implements PlayerMatchService {
    @Autowired
    PlayerMatchDao playerMatchDao;
    @Override
    public List<PlayerRankDto> rank10Player(){
        return playerMatchDao.rank10Player();
    }

}
