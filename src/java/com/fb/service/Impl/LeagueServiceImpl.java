package com.fb.service.Impl;

import com.fb.DAO.LeagueDao;
import com.fb.entity.League;
import com.fb.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sm on 7/1/17.
 */
@Service("LeagueService")
@Transactional
public class LeagueServiceImpl implements LeagueService{
    @Autowired
    LeagueDao leagueDao;

    @Override
    public League findLeagueById(Integer id) {
        return leagueDao.findLeagueById(id);
    }

    @Override
    public void addLeague(League e) {
        leagueDao.addLeague(e);
    }

    @Override
    public void deleteLeagueById(Integer id) {
        leagueDao.deleteLeagueById(id);
    }

    @Override
    public void updateLeague(League e) {
        leagueDao.updateLeague(e);
    }

    @Override
    public List<League> findAllLeague() {
        return leagueDao.findAllLeague();
    }
}
