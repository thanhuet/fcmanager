package com.fb.service.Impl;

import com.fb.DAO.MatchDao;
import com.fb.DAO.PlayerDao;
import com.fb.DAO.TeamDao;
import com.fb.DTO.TeamDto;
import com.fb.entity.Player;
import com.fb.entity.Team;
import com.fb.service.TeamService;
import com.fb.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 7/4/17.
 */
@Service("TeamService")
@Transactional
public class TeamServiceImpl implements TeamService {
    @Autowired
    TeamDao teamDao;
    @Autowired
    MatchDao matchDao;
    @Autowired
    PlayerDao playerDao;

    @Override
    public TeamDto findTeamById(Integer id) {
        Team team=teamDao.findTeamById(id);
        return convertToDTO(team);
    }

    @Override
    public void addTeam(TeamDto teamDto) {
        String name=teamDto.getName().trim();
        String coach=teamDto.getCoach().trim();
        teamDao.addTeam(new Team(name,coach));
    }

    @Override
    public void deleteTeamById(Integer id) {
        matchDao.deleteMatchByTeam(id);
        playerDao.setNullForPlayer(id);
        //
        teamDao.deleteTeamById(id);
    }

    @Override
    public void updateTeam(TeamDto teamDto) {
        Team team=convertToModel(teamDto);
        teamDao.updateTeam(team);
    }

    @Override
    public List<TeamDto> findAllTeam() {
        List<Team> teams = teamDao.findAllTeam();
        return convertToDTO(teams);
    }

    private TeamDto convertToDTO(Team team){
        TeamDto teamDto=new TeamDto();
        teamDto.setId(team.getId());
        teamDto.setName(team.getName());
        Player captainObject=team.getCaptain();
        String captain=null;
        int captain_id= 0;
        if(captainObject!=null){
            captain=captainObject.getName();
            captain_id=captainObject.getId();
        }
        teamDto.setCaptian(captain);
        teamDto.setCaptian_id(captain_id);
        teamDto.setCoach(team.getCoach());
        teamDto.setNumberPlayer(playerDao.findNumberPlayersOfTeam(team));
        return teamDto;
    }

    private List<TeamDto> convertToDTO(List<Team> teams){
        List<TeamDto> teamDtos=new ArrayList<>();
        for(Team team:teams){
            teamDtos.add(convertToDTO(team));
        }
        return teamDtos;
    }

    private Team convertToModel(TeamDto teamDto){
        Team team= teamDao.findTeamById(teamDto.getId());
        team.setName(teamDto.getName());
        team.setCaptain(playerDao.findPlayerById(teamDto.getCaptian_id()));
        team.setCoach(teamDto.getCoach());
        return team;
    };
}
