package com.fb.service.Impl;

import com.fb.DAO.PlayerDao;
import com.fb.DAO.PlayerMatchDao;
import com.fb.DAO.TeamDao;
import com.fb.DTO.PlayerDto;
import com.fb.entity.Player;
import com.fb.entity.Team;
import com.fb.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 7/5/17.
 */
@Service("PlayerService")
@Transactional
public class PlayerSerivceImpl implements PlayerService {

    @Autowired
    PlayerDao playerDao;
    @Autowired
    TeamDao teamDao;
    @Autowired
    PlayerMatchDao playerMatchDao;

    @Override
    public PlayerDto findPlayerById(Integer id) {
        Player player = playerDao.findPlayerWithTeamById(id);
        return convertToDTO(player);
    }

    @Override
    public void addPlayer(PlayerDto playerDto) {

        String name = playerDto.getName();
        short number=playerDto.getNumber();
        int age=playerDto.getAge();
        short height = playerDto.getHeight();
        short weight = playerDto.getWeight();
        Date joined = playerDto.getJoined();
        String position = playerDto.getPosition();
        int team_id = playerDto.getTeam_id();
        Team team = teamDao.findTeamById(team_id);
        playerDao.addPlayer(new Player(name,number,age,height,weight,joined,team,position));
    }

    @Override
    public void deletePlayerById(Integer id) {
        playerMatchDao.deleteMatchPlayerByPlayer(id);
        //
        playerDao.deletePlayerById(id);
    }

    @Override
    public void updatePlayer(PlayerDto playerDto) {
        Player player =convertToModel(playerDto);
        playerDao.updatePlayer(player);
    }

    @Override
    public List<PlayerDto> findAllPlayer() {
        List<Player> players = playerDao.findAllPlayer();
        return convertToDTO(players);
    }

    @Override
    public List<PlayerDto> findPlayersOfTeam(int teamId) {
        Team team = teamDao.findTeamById(teamId);
        List<Player> players = playerDao.findPlayersOfTeam(team);
        return convertToDTO(players);
    }

    @Override
    public Long findNumberPlayersOfTeam(Team team) {
        return playerDao.findNumberPlayersOfTeam(team);
    }

    private PlayerDto convertToDTO(Player player) {
        PlayerDto playerDto=new PlayerDto();
        playerDto.setId(player.getId());
        playerDto.setName(player.getName());
        playerDto.setNumber(player.getNumber());
        playerDto.setAge(player.getAge());
        playerDto.setHeight(player.getHeight());
        playerDto.setWeight(player.getWeight());
        playerDto.setJoined(player.getJoined());
        playerDto.setPosition(player.getPosition());
        playerDto.setTeam_id(player.getTeam().getId());
        playerDto.setTeam_name(player.getTeam().getName());
        return playerDto;
    }

    private List<PlayerDto> convertToDTO(List<Player> players) {
        List<PlayerDto> playerDtos = new ArrayList<>();
        for (Player player : players) {
            playerDtos.add(convertToDTO(player));
        }
        return playerDtos;
    }

    private Player convertToModel(PlayerDto playerDto) {
        Player player=playerDao.findPlayerById(playerDto.getId());
        player.setName(playerDto.getName());
        player.setNumber(playerDto.getNumber());
        player.setAge(playerDto.getAge());
        player.setHeight(playerDto.getHeight());
        player.setWeight(playerDto.getWeight());
        player.setJoined(playerDto.getJoined());
        player.setPosition(playerDto.getPosition());
        int team_id = playerDto.getTeam_id();
        player.setTeam(teamDao.findTeamById(team_id));
        return player;
    }

    private Boolean checkDto(PlayerDto playerDto){
        return true;
    }



}
