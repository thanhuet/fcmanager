package com.fb.service.Impl;

import com.fb.DAO.RoundDao;
import com.fb.entity.Round;
import com.fb.entity.Season;
import com.fb.service.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by sm on 7/6/17.
 */
@Repository("RoundService")
@Transactional
public class RoundServiceImpl implements RoundService {
    @Autowired
    RoundDao roundDao;


    @Override
    public Round findRoundById(Integer id) {
        return roundDao.findRoundWithMatchById(id);
    }

    @Override
    public void addRound(Round round) {
        roundDao.addRound(round);
    }

    @Override
    public void deleteRoundById(Integer id) {
        roundDao.deleteRoundById(id);
    }

    @Override
    public void updateRound(Round round) {
        roundDao.updateRound(round);
    }

    @Override
    public List<Round> findAllRound() {
        return roundDao.findAllRound();
    }
}
