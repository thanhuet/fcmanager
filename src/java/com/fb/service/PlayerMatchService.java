package com.fb.service;


import com.fb.DTO.PlayerRankDto;
import com.fb.entity.PlayerMatch;

import java.util.List;

/**
 * Created by sm on 7/9/17.
 */
public interface PlayerMatchService {
    List<PlayerRankDto> rank10Player() ;
}
