package com.fb.service;

import com.fb.DTO.MatchDto;
import com.fb.DTO.PlayerDto;
import com.fb.entity.Match;

import java.util.List;

/**
 * Created by sm on 7/5/17.
 */
public interface MatchService {
    MatchDto findMatchById(Integer id);
    void addMatch(MatchDto matchDto);
    void deleteMatchById(Integer id);
    void updateMatch(MatchDto matchDto);
    List<MatchDto> findAllMatch();
    List<MatchDto> convertToDTO (List<Match> matches);
}
