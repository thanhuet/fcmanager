package com.fb.service;

import com.fb.DTO.TeamDto;
import com.fb.entity.Team;

import java.util.List;

/**
 * Created by sm on 7/4/17.
 */
public interface TeamService {
    TeamDto findTeamById(Integer id);
    void addTeam(TeamDto teamDto);
    void deleteTeamById(Integer id);
    void updateTeam(TeamDto teamDto);
    List<TeamDto> findAllTeam();
}
