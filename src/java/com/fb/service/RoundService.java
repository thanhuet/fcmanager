package com.fb.service;


import com.fb.entity.Round;

import java.util.List;

/**
 * Created by sm on 7/6/17.
 */
public interface RoundService {
    Round findRoundById(Integer id);
    void addRound(Round round);
    void deleteRoundById(Integer id);
    void updateRound(Round round);
    List<Round> findAllRound();
}
