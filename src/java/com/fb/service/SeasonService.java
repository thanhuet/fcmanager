package com.fb.service;

import com.fb.entity.Season;

import java.util.List;

/**
 * Created by sm on 7/6/17.
 */
public interface SeasonService {
    Season findSeasonById(Integer id);
    void addSeason(Season season);
    void deleteSeasonById(Integer id);
    void updateSeason(Season season);
    List<Season> findAllSeason();
}
