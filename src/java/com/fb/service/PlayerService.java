package com.fb.service;


import com.fb.DTO.PlayerDto;
import com.fb.entity.Team;

import java.util.List;

/**
 * Created by sm on 7/5/17.
 */
public interface PlayerService {
    PlayerDto findPlayerById(Integer id);
    void addPlayer(PlayerDto playerDto);
    void deletePlayerById(Integer id);
    void updatePlayer(PlayerDto playerDto);
    List<PlayerDto> findAllPlayer();
    List<PlayerDto> findPlayersOfTeam(int teamId);
    Long findNumberPlayersOfTeam(Team team);
}
