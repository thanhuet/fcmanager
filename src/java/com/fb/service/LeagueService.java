package com.fb.service;

import com.fb.entity.League;

import java.util.List;

/**
 * Created by sm on 7/1/17.
 */
public interface LeagueService {
    League findLeagueById(Integer id);
    void addLeague(League e);
    void deleteLeagueById(Integer id);
    void updateLeague(League e);
    List<League> findAllLeague();
}
