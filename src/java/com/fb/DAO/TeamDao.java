package com.fb.DAO;

import com.fb.entity.Team;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface TeamDao {
    Team findTeamById(Integer id);
    void addTeam(Team e);
    void deleteTeamById(Integer id);
    void updateTeam(Team e);
    List<Team> findAllTeam();
}
