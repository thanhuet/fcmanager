package com.fb.DAO;


import com.fb.entity.Match;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface MatchDao {
    Match findMatchById(Integer id);
    void addMatch(Match e);
    void deleteMatchById(Integer id);
    void updateMatch(Match e);
    List<Match> findAllMatch();
    void deleteMatchByTeam(int teamId);
}
