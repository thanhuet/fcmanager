package com.fb.DAO;

import com.fb.entity.Season;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface SeasonDao {
    Season findSeasonById(Integer id);
    Season findSeasonWithRoundById(Integer id);
    void addSeason(Season e);
    void deleteSeasonById(Integer id);
    void updateSeason(Season e);
    List<Season> findAllSeason();
}
