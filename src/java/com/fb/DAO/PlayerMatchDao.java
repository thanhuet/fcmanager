package com.fb.DAO;

import com.fb.DTO.PlayerRankDto;
import com.fb.entity.PlayerMatch;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface PlayerMatchDao {
    public void deleteMatchPlayerByPlayer(int playerId);
    public void deleteMatchPlayerByMatch(int matchId);
    List<PlayerRankDto> rank10Player();
}
