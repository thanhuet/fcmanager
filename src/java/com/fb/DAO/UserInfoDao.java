package com.fb.DAO;

import com.fb.entity.UserInfo;

/**
 * Created by sm on 7/2/17.
 */
public interface UserInfoDao {
    UserInfo findUserInfo(String userName);
}
