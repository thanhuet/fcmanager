package com.fb.DAO;

import com.fb.entity.Player;
import com.fb.entity.Team;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface PlayerDao {
    Player findPlayerById(Integer id);
    Player findPlayerWithTeamById(Integer id);
    void addPlayer(Player e);
    void deletePlayerById(Integer id);
    void updatePlayer(Player e);
    List<Player> findAllPlayer();
    List<Player> findPlayersOfTeam(Team team);
    Long findNumberPlayersOfTeam(Team team);

    void setNullForPlayer(int team_id);
}
