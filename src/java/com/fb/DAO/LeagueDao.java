package com.fb.DAO;

import com.fb.entity.League;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface LeagueDao  {
    League findLeagueById(Integer id);
    void addLeague(League e);
    void deleteLeagueById(Integer id);
    void updateLeague(League e);
    List<League> findAllLeague();

}
