package com.fb.DAO;


import com.fb.entity.Round;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
public interface RoundDao {
    Round findRoundById(Integer id);
    Round findRoundWithMatchById(Integer id);
    void addRound(Round e);
    void deleteRoundById(Integer id);
    void updateRound(Round e);
    List<Round> findAllRound();
}
