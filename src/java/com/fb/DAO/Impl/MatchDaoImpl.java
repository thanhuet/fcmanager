package com.fb.DAO.Impl;

import com.fb.DAO.MatchDao;
import com.fb.entity.Match;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("MatchDao")
public class MatchDaoImpl extends BaseEntityDaoImpl<Integer,Match> implements MatchDao {
    @Override
    public Match findMatchById(Integer id) {
        return findById(id);
    }

    @Override
    public void addMatch(Match e) {
        add(e);
    }

    @Override
    public void deleteMatchById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updateMatch(Match e) {
        update(e);
    }

    @Override
    public List<Match> findAllMatch() {
        return findAll();
    }

    @Override
    public void deleteMatchByTeam(int teamId) {
        Session session=getSession();
        String hql ="DELETE FROM matches where home_team_id = :idHome OR away_team_id= :idAway ";
        Query query=session.createSQLQuery(hql);
        query.setParameter("idHome",teamId);
        query.setParameter("idAway",teamId);
        query.executeUpdate();
    }


}
