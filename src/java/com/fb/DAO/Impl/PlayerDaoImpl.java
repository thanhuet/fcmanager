package com.fb.DAO.Impl;

import com.fb.DAO.PlayerDao;
import com.fb.DAO.TeamDao;
import com.fb.entity.Player;
import com.fb.entity.Team;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("PlayerDao")
public class PlayerDaoImpl extends BaseEntityDaoImpl<Integer, Player> implements PlayerDao {

    @Override
    public Player findPlayerById(Integer id) {
        return findById(id);
    }

    @Override
    public Player findPlayerWithTeamById(Integer id) {
        Player player = findById(id);
        Hibernate.initialize(player.getTeam());
        return player;
    }

    @Override
    public void addPlayer(Player e) {
        add(e);
    }

    @Override
    public void deletePlayerById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updatePlayer(Player e) {
        update(e);
    }

    @Override
    public List<Player> findAllPlayer() {
        return findAll();
    }

    @Override
    public List<Player> findPlayersOfTeam(Team team) {
        String hql = "FROM Player P WHERE P.team  = :team";
        Query query = getSession().createQuery(hql);
        query.setParameter("team", team);
        List<Player> players = query.list();
        return players;
    }

    @Override
    public Long findNumberPlayersOfTeam(Team team) {
        Query query = getSession().createQuery(
                "select COUNT(*) from Player p where p.team = :team");
        query.setParameter("team",team);
        Long count = (Long)query.uniqueResult();
        return count;
    }


    @Override
    public void setNullForPlayer(int team_id) {
        Session session = getSession();
        String hql = "update players SET team_id =null where team_id = :id ";
        Query query = session.createSQLQuery(hql);
        query.setParameter("id", team_id);
        query.executeUpdate();
    }

}
