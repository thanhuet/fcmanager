package com.fb.DAO.Impl;

import com.fb.DAO.MatchDao;
import com.fb.DAO.PlayerDao;
import com.fb.DAO.PlayerMatchDao;
import com.fb.DTO.PlayerRankDto;
import com.fb.entity.Player;
import com.fb.entity.PlayerMatch;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("PlayerMatchDaoImpl")
@Transactional
public class PlayerMatchDaoImpl extends BaseEntityDaoImpl<Integer,PlayerMatch> implements PlayerMatchDao{
    @Autowired
    PlayerDao playerDao;
    @Autowired
    MatchDao matchDao;

    @Override
    public void deleteMatchPlayerByPlayer(int playerId) {
        Session session=getSession();
        String hql ="DELETE FROM player_match where player_id = :id ";
        Query query=session.createSQLQuery(hql);
        query.setParameter("id",playerId);
        query.executeUpdate();
    }

    @Override
    public void deleteMatchPlayerByMatch(int matchId) {
        Session session=getSession();
        String hql ="DELETE FROM player_match where match_id = :id ";
        Query query=session.createSQLQuery(hql);
        query.setParameter("id",matchId);
        query.executeUpdate();
    }

    @Override
    public List<PlayerRankDto> rank10Player() {
        Session session=getSession();
        String hql ="SELECT player_id playerId,SUM(goal_number) totalGoal FROM player_match GROUP BY player_id ORDER BY totalGoal DESC LIMIT 10;";
        Query query=session.createSQLQuery(hql)
                .addScalar("playerId")
                .addScalar("totalGoal")
                .setResultTransformer(Transformers.aliasToBean(PlayerRankDto.class));
        List<PlayerRankDto> list=query.list();
        for(PlayerRankDto prd :list){
            prd.setPlayer(playerDao.findPlayerById(prd.getPlayerId()));
        }
        return list;
    }
}
