package com.fb.DAO.Impl;

import com.fb.DAO.SeasonDao;
import com.fb.entity.Round;
import com.fb.entity.Season;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("SeasonDao")
public class SeasonDaoImpl extends BaseEntityDaoImpl<Integer,Season> implements SeasonDao {
    @Override
    public Season findSeasonById(Integer id) {
        return findById(id);
    }

    @Override
    public Season findSeasonWithRoundById(Integer id) {
        Season season=findById(id);
        Hibernate.initialize(season.getRounds());
        return season;
    }

    @Override
    public void addSeason(Season e) {
        add(e);
    }

    @Override
    public void deleteSeasonById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updateSeason(Season e) {
        update(e);
    }

    @Override
    public List<Season> findAllSeason() {
        return findAllSeason();
    }
}
