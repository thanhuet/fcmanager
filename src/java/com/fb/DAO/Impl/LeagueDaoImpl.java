package com.fb.DAO.Impl;

import com.fb.DAO.LeagueDao;
import com.fb.entity.League;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("LeagueDao")
public class LeagueDaoImpl extends BaseEntityDaoImpl<Integer,League> implements LeagueDao {

    protected LeagueDaoImpl() {
    }

    @Override
    public League findLeagueById(Integer id) {
        return  findById(id);
    }

    @Override
    public void addLeague(League e) {
        add(e);
    }

    @Override
    public void deleteLeagueById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updateLeague(League e) {
        update(e);
    }

    @Override
    public List<League> findAllLeague() {
        return findAll();
    }
}
