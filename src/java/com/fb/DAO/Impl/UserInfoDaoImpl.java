package com.fb.DAO.Impl;

import com.fb.DAO.UserInfoDao;
import com.fb.entity.UserInfo;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by sm on 7/2/17.
 */
@Repository("UserInfoDao")
@Transactional
public class UserInfoDaoImpl implements UserInfoDao {
    @Autowired
    private SessionFactory sessionFactory;

    public UserInfoDaoImpl() {

    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public UserInfo findUserInfo(String userName) {

        String hql="from UserInfo where userName= :username";
        Session session = getSession();

        Query query = session.createQuery(hql);
        query.setParameter("username", userName);

        return (UserInfo) query.uniqueResult();
    }

//    public List<String> getUserRoles(String userName) {
//        String sql = "Select r.userRole "//
//                + " from " + UserRole.class.getName() + " r where r.user.username = :username ";
//
//        Session session = sessionFactory.getCurrentSession();
//
//        Query query = session.createQuery(sql);
//        query.setParameter("username", userName);
//
//        List<String> roles = query.list();
//
//        return roles;
//    }
}
