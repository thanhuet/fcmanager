package com.fb.DAO.Impl;

import com.fb.DAO.RoundDao;
import com.fb.entity.Round;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("RoundDao")
public class RoundDaoImpl extends BaseEntityDaoImpl<Integer,Round> implements RoundDao{
    @Override
    public Round findRoundById(Integer id) {
        return findById(id);
    }

    @Override
    public Round findRoundWithMatchById(Integer id) {
        Round round =findRoundById(id);
        Hibernate.initialize(round.getMatches());
        return round;
    }

    @Override
    public void addRound(Round e) {
        add(e);
    }

    @Override
    public void deleteRoundById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updateRound(Round e) {
        update(e);
    }

    @Override
    public List<Round> findAllRound() {
        return findAll();
    }

}
