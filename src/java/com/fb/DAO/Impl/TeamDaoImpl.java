package com.fb.DAO.Impl;

import com.fb.DAO.TeamDao;
import com.fb.entity.League;
import com.fb.entity.Team;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("TeamDao")
public class TeamDaoImpl extends BaseEntityDaoImpl<Integer,Team> implements TeamDao {
    @Override
    public Team findTeamById(Integer id) {
        return  findById(id);
    }

    @Override
    public void addTeam(Team e) {
        add(e);
    }

    @Override
    public void deleteTeamById(Integer id) {
        deleteById(id);
    }

    @Override
    public void updateTeam(Team e) {
        update(e);
    }

    @Override
    public List<Team> findAllTeam() {
        return findAll();
    }
}
