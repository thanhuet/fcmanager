package com.fb.DAO.Impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Repository("BaseEntityDao")
abstract class BaseEntityDaoImpl<key extends Serializable,E> {
    @Autowired
    private SessionFactory sessionFactory;

    private final Class<E> persistentClass;

    protected BaseEntityDaoImpl() {
        this.persistentClass =(Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public List<E> findAll() {
        String hql="From "+persistentClass.getName();
        Query query = getSession().createQuery(hql);
        List results = query.list();
        return results;
    }

    public E findById(key id) {
        return (E) getSession().get(persistentClass, id);
    }

    public void add(E e) {
        getSession().save(e);
    }

    public void deleteById(key id) {
        String hql = "DELETE FROM "  +persistentClass.getName()+ " WHERE id = :id";
        Query query = getSession().createQuery(hql);
        query.setParameter("id", id);
        int result = query.executeUpdate();
    }

    public void update(E e) {
//        getSession().saveOrUpdate(e);
        getSession().update(e);
    }
}
