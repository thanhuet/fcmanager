package com.fb.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "session_team")
@IdClass(SeasonTeamId.class)
public class SeasonTeam implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "season_id", nullable = false)
    private Season season;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;


    public SeasonTeam(Season season, Team team) {
        this.season = season;
        this.team = team;
    }

    public SeasonTeam() {
    }


    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
