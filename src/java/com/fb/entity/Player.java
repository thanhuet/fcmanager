package com.fb.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */

enum Position{Midfielde,Goalkeeper,Defender,Attacking};

@Entity
@Table(name = "players")
public class Player extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "number_player")
    private short number;
    @Column(name = "age")
    private int age;
    @Column(name = "height")
    private short height;
    @Column(name = "weight")
    private short weight;
    @Column(name = "joined")
    private Date joined;
    @Column(name = "position")
    private String position;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;
    @OneToMany(mappedBy = "player")
    private List<PlayerMatch> playerMatches = new ArrayList<>();

    public Player() {
    }

    public Player(String name, int age, short height, short weight, Date joined, String position){
        this.name = name;
        this.age=age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.position = position;
    }

    public Player(String name, short number ,int age, short height, short weight, Date joined,Team team, String position){
        this.name = name;
        this.number=number;
        this.age=age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.team=team;
        this.position = position;
    }

    public Player(int id, String name, int age, short height, short weight, Date joined, Team team, String position) {
        this.id = id;
        this.name = name;
        this.age=age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.position = position;
        this.team=team;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public short getHeight() {
        return height;
    }

    public void setHeight(short height) {
        this.height = height;
    }

    public short getWeight() {
        return weight;
    }

    public void setWeight(short weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<PlayerMatch> getPlayerMatches() {
        return playerMatches;
    }

    public void setPlayerMatches(List<PlayerMatch> playerMatches) {
        this.playerMatches = playerMatches;
    }

    public short getNumber() {
        return number;
    }

    public void setNumber(short number) {
        this.number = number;
    }
}
