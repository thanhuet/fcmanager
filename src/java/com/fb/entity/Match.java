package com.fb.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "matches")
public class Match  extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "home_team_id")
    private Team homeTeam;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "away_team_id")
    private Team awayTeam;
    @Column(name = "start_time")
    private Date startTime;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "round_id")
    private Round round;
    @Column(name = "home_team_score")
    private int homeGoal;
    @Column(name = "away_team_score")
    private int awayGoal;
    @OneToMany(mappedBy = "match")
    private List<PlayerMatch> playerMatches=new ArrayList<>();
    @Column(name = "description")
    private String description;

    public Match(int id, Team homeTeam, Team awayTeam, Date startTime, Round round) {
        this.id = id;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = startTime;
        this.round = round;
    }

    public Match() {
    }

    public Match(int id, Team homeTeam, Team awayTeam, Date startTime, Round round, String description) {
        this.id=id;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = startTime;
        this.round = round;
        this.description=description;
    }

    public Match( Team homeTeam, Team awayTeam, Date startTime, Round round, String description) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = startTime;
        this.round = round;
        this.description=description;
    }

    public Match( int id,Team homeTeam, Team awayTeam, Date startTime, Round round,int homeGoal,int awayGoal, String description) {
        this.id=id;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = startTime;
        this.round = round;
        this.homeGoal=homeGoal;
        this.awayGoal=awayGoal;
        this.description=description;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public List<PlayerMatch> getPlayerMatches() {
        return playerMatches;
    }

    public void setPlayerMatches(List<PlayerMatch> playerMatches) {
        this.playerMatches = playerMatches;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHomeGoal() {
        return homeGoal;
    }

    public void setHomeGoal(int homeGoal) {
        this.homeGoal = homeGoal;
    }

    public int getAwayGoal() {
        return awayGoal;
    }

    public void setAwayGoal(int awayGoal) {
        this.awayGoal = awayGoal;
    }
}
