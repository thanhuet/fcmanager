package com.fb.entity;

import java.io.Serializable;

/**
 * Created by sm on 6/30/17.
 */
public class SeasonTeamId implements Serializable {
    private Season season;
    private Team team;

    public SeasonTeamId(Season season, Team team) {
        this.season = season;
        this.team = team;
    }

    public SeasonTeamId() {
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
