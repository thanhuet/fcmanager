package com.fb.entity;

import jdk.nashorn.internal.scripts.JO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "teams")
public class Team extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "captain_id")
    private Player captain;

    @Column(name = "coach")
    private String coach;

    @OneToMany(mappedBy = "team",fetch = FetchType.LAZY)
    private List<Player> players= new ArrayList<>();

    @OneToMany(mappedBy = "team",fetch = FetchType.LAZY)
    private List<SeasonTeam> sessionTeams= new ArrayList<>();

    @OneToMany(mappedBy = "homeTeam",fetch = FetchType.LAZY)
    private List<Match> homeMatches= new ArrayList<>();

    @OneToMany(mappedBy = "awayTeam",fetch = FetchType.LAZY)
    private List<Match> awayMatches= new ArrayList<>();

    public Team() {
    }
    public Team(int id, String name, Player captain,String coach) {
        this.id = id;
        this.name = name;
        this.captain = captain;
        this.coach=coach;
    }

    public Team(int id, String name, String coach) {
        this.id = id;
        this.name = name;
        this.coach=coach;
    }

    public Team(String name,Player captain,String coach){
        this.name=name;
        this.captain=captain;
        this.coach=coach;
    }

    public Team(String name,String coach){
        this.name=name;
        this.coach=coach;
    }

    public Team(String name){
        this.name=name;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getCaptain() {
        return captain;
    }

    public void setCaptain(Player captain) {
        this.captain = captain;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }



    public List<SeasonTeam> getSessionTeams() {
        return sessionTeams;
    }

    public void setSessionTeams(List<SeasonTeam> sessionTeams) {
        this.sessionTeams = sessionTeams;
    }

    public List<Match> getHomeMatches() {
        return homeMatches;
    }

    public void setHomeMatches(List<Match> homeMatches) {
        this.homeMatches = homeMatches;
    }

    public List<Match> getAwayMatches() {
        return awayMatches;
    }

    public void setAwayMatches(List<Match> awayMatches) {
        this.awayMatches = awayMatches;
    }
}
