package com.fb.entity;

import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "leagues")
public class League extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id",nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "league", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Season> seasons =new ArrayList<>();

    public League(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public League() {
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
