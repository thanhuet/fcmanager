package com.fb.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "player_match")
public class PlayerMatch extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "match_id")
    private Match match;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;
    @Column(name = "yellow_card")
    private short yellowCard;
    @Column(name = "red_card")
    private short redCard;
    @Column(name = "goal_number")
    private short goalNumber;
    @Column(name = "own_goal_number")
    private short ownGoalNumber;

    public PlayerMatch(int id, Match match, Player player, short yellowCard, short redCard, short goalNumber, short ownGoalNumber) {
        this.id = id;
        this.match = match;
        this.player = player;
        this.yellowCard = yellowCard;
        this.redCard = redCard;
        this.goalNumber = goalNumber;
        this.ownGoalNumber = ownGoalNumber;
    }

    public PlayerMatch() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public short getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(short yellowCard) {
        this.yellowCard = yellowCard;
    }

    public short getRedCard() {
        return redCard;
    }

    public void setRedCard(short redCard) {
        this.redCard = redCard;
    }

    public short getGoalNumber() {
        return goalNumber;
    }

    public void setGoalNumber(short goalNumber) {
        this.goalNumber = goalNumber;
    }

    public short getOwnGoalNumber() {
        return ownGoalNumber;
    }

    public void setOwnGoalNumber(short ownGoalNumber) {
        this.ownGoalNumber = ownGoalNumber;
    }


}
