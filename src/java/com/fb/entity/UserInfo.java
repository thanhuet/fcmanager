package com.fb.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sm on 7/2/17.
 */
 enum Role{USER, ADMIN}

@Entity
@Table(name = "users")
public class UserInfo extends BaseEntity implements Serializable{
    @Id
    @Column(name = "id", nullable = false )
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @Column(name = "username")
    private String userName;
    @Column(name = "password")
    private String password;
    @Column(name = "role")
    private String role;
    public UserInfo()  {

    }

    public UserInfo(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String  getRole() {
        return role;
    }

    public void setRole(String  role) {
        this.role = role;
    }
}
