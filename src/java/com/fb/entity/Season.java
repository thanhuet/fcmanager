package com.fb.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sm on 6/30/17.
 */
@Entity
@Table(name = "seasons")
public class Season extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id",nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "league_id",nullable = false)
    private League league;
    @OneToMany(mappedBy = "season")
    private List<SeasonTeam> sessionTeams=new ArrayList<>();
    @OneToMany(mappedBy = "season")
    private List<Round> rounds=new ArrayList<>();


    public Season() {
    }

    public Season(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<SeasonTeam> getSessionTeams() {
        return sessionTeams;
    }

    public void setSessionTeams(List<SeasonTeam> sessionTeams) {
        this.sessionTeams = sessionTeams;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public List<Round> getRounds() {
        return rounds;
    }
}
