<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Football Manager </title>

    <!-- Bootstrap -->
    <link  href="<c:url value="/vendors/bootstrap/dist/css/bootstrap.min.css"/>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<c:url value="/vendors/font-awesome/css/font-awesome.min.css"/>"  rel="stylesheet">
    <!-- NProgress -->
    <link href="<c:url value="/vendors/nprogress/nprogress.css"/>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<c:url value="/vendors/iCheck/skins/flat/green.css"/>" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<c:url value="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"/>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<c:url value="/vendors/jqvmap/dist/jqvmap.min.css"/>" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<c:url value="/vendors/bootstrap-daterangepicker/daterangepicker.css"/>" rel="stylesheet">

    <link href="<c:url value="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"/>" rel="stylesheet"/>
    
    <!-- Custom Theme Style -->
    <link href="<c:url value="/build/css/custom.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/build/css/myCustom.css"/>" rel="stylesheet">

    <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/> "/>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <%@ include  file="./partials/menuLeft.jsp" %>

        <!-- top navigation -->
        <div class="top_nav">
          <%@ include  file="./partials/menuTop.jsp" %>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="my_right_col " role="main">
          <form:form id="demo-form2" action="/teams/edit-team" method="post" modelAttribute="editteam" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
            <form:input type="hidden" id="id" name="id" required="required" class="form-control col-md-7 col-xs-12" path="id" />
            <div class="form-group">
              <form:label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" style="color:#f7f7f9" path="name">Name <span class="required">*</span></form:label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <form:input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" path="name"/>
              </div>
            </div>
            <div class="form-group">
              <form:label for="captian_id" class="control-label col-md-3 col-sm-3 col-xs-12" style="color:#f7f7f9" path="captian_id">Đội Trưởng</form:label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <form:select path="captian_id" class="form-control col-md-7 col-xs-12">
                  <c:forEach items="${listplayer}" var="player">
                    <form:option value="${player.id}" >${player.name}</form:option>
                  </c:forEach>
                </form:select>
              </div>
            </div>
            <div class="form-group">
              <form:label for="coach" class="control-label col-md-3 col-sm-3 col-xs-12" style="color:#f7f7f9" path="coach">Coach / Initial</form:label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <form:input id="coach" class="form-control col-md-7 col-xs-12" type="text" name="coach" path="coach"/>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="/teams" class="btn btn-primary" type="button">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </form:form>


        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Football Manager - Bootstrap Admin Template by <a href="/">Football Manager</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<c:url value="/vendors/jquery/dist/jquery.min.js"/>"> </script>
    <!-- Bootstrap -->
    <script src="<c:url value="/vendors/bootstrap/dist/js/bootstrap.min.js"/>"></script>
    <!-- FastClick -->
    <script src="<c:url value="/vendors/fastclick/lib/fastclick.js"/>"></script>
    <!-- NProgress -->
    <script src="<c:url value="/vendors/nprogress/nprogress.js"/>"></script>
    <!-- Chart.js -->
    <script src="<c:url value="/vendors/Chart.js/dist/Chart.min.js"/>"></script>
    <!-- gauge.js -->
    <script src="<c:url value="/vendors/gauge.js/dist/gauge.min.js"/>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<c:url value="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"/>"></script>
    <!-- iCheck -->
    <script src="<c:url value="/vendors/iCheck/icheck.min.js"/>"></script>
    <!-- Skycons -->
    <script src="<c:url value="/vendors/skycons/skycons.js"/>"></script>
    <!-- Flot -->
    <script src="<c:url value="/vendors/Flot/jquery.flot.js"/>"></script>
    <script src="<c:url value="/vendors/Flot/jquery.flot.pie.js"/>"></script>
    <script src="<c:url value="/vendors/Flot/jquery.flot.time.js"/>"></script>
    <script src="<c:url value="/vendors/Flot/jquery.flot.stack.js"/>"></script>
    <script src="<c:url value="/vendors/Flot/jquery.flot.resize.js"/>"></script>
    <!-- Flot plugins -->
    <script src="<c:url value="/vendors/flot.orderbars/js/jquery.flot.orderBars.js"/>"></script>
    <script src="<c:url value="/vendors/flot-spline/js/jquery.flot.spline.min.js"/>"></script>
    <script src="<c:url value="/vendors/flot.curvedlines/curvedLines.js"/>"></script>
    <!-- DateJS -->
    <script src="<c:url value="/vendors/DateJS/build/date.js"/>"></script>
    <!-- JQVMap -->
    <script src="<c:url value="/vendors/jqvmap/dist/jquery.vmap.js"/>"></script>
    <script src="<c:url value="/vendors/jqvmap/dist/maps/jquery.vmap.world.js"/>"></script>
    <script src="<c:url value="/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"/>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<c:url value="/vendors/moment/min/moment.min.js"/>"></script>
    <script src="<c:url value="/vendors/bootstrap-daterangepicker/daterangepicker.js"/>"></script>
    <!-- jQuery custom content scroller -->
    <script  src="<c:url value="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"/>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<c:url value="/build/js/custom.min.js"/>"></script>
	
  </body>
</html>
