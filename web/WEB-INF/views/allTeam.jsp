<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Football Manager </title>

    <!-- Bootstrap -->
    <link href="<c:url value="/vendors/bootstrap/dist/css/bootstrap.min.css"/>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<c:url value="/vendors/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<c:url value="/vendors/nprogress/nprogress.css"/>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<c:url value="/vendors/iCheck/skins/flat/green.css"/>" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="<c:url value="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"/>"
          rel="stylesheet">
    <!-- JQVMap -->
    <link href="<c:url value="/vendors/jqvmap/dist/jqvmap.min.css"/>" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<c:url value="/vendors/bootstrap-daterangepicker/daterangepicker.css"/>" rel="stylesheet">

    <link href="<c:url value="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"/>"
          rel="stylesheet"/>

    <!-- Datatables -->
    <link href="<c:url value="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"/>"
          rel="stylesheet">
    <link href="<c:url value="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"/>"
          rel="stylesheet">
    <link href="<c:url value="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<c:url value="/build/css/custom.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/build/css/myCustom.css"/>" rel="stylesheet">

    <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/> "/>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <%@ include  file="./partials/menuLeft.jsp" %>

        <!-- top navigation -->
        <div class="top_nav">
            <%@ include  file="./partials/menuTop.jsp" %>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="my_right_col " role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Danh Sách Đội Bóng
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        Thận Trọng khi Chỉnh Sửa Thông Tin Đội Bóng
                    </p>
                    <a href="/teams/add/" class="btn btn-success"><i class="fa fa-pencil"></i> ADD </a>
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên Đội Bóng</th>
                            <th>Đội Trưởng</th>
                            <th>Huấn Luyện Viên</th>
                            <th> Cầu Thủ</th>
                            <th> Giải Đấu Tham Gia</th>
                            <th>Sửa/Xóa</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% int pos=0; %>
                        <c:forEach var="team" items="${listteam}">
                            <tr>
                                <td><%=pos++%></td>
                                <td>
                                    <a href="/teams/${team.id}">
                                        ${team.name}
                                    </a>
                                </td>
                                <td>
                                    <a href="/players/${team.captian_id}">
                                        ${team.captian}
                                    </a>
                                </td>
                                <td>${team.coach}</td>
                                <td>${team.numberPlayer}</td>
                                <td>1</td>
                                <td>
                                    <a href="/teams/edit/${team.id}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="/teams/delete/${team.id}" class="btn btn-danger btn-xs" onclick="confirmDelete()"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Football Manager - Bootstrap Admin Template by <a href="/">Football Manager</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<c:url value="/vendors/jquery/dist/jquery.min.js"/>"></script>
<!-- Bootstrap -->
<script src="<c:url value="/vendors/bootstrap/dist/js/bootstrap.min.js"/>"></script>
<!-- FastClick -->
<script src="<c:url value="/vendors/fastclick/lib/fastclick.js"/>"></script>
<!-- NProgress -->
<script src="<c:url value="/vendors/nprogress/nprogress.js"/>"></script>
<!-- Chart.js -->
<script src="<c:url value="/vendors/Chart.js/dist/Chart.min.js"/>"></script>
<!-- gauge.js -->
<script src="<c:url value="/vendors/gauge.js/dist/gauge.min.js"/>"></script>
<!-- bootstrap-progressbar -->
<script src="<c:url value="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"/>"></script>
<!-- iCheck -->
<script src="<c:url value="/vendors/iCheck/icheck.min.js"/>"></script>
<!-- Skycons -->
<script src="<c:url value="/vendors/skycons/skycons.js"/>"></script>
<!-- Flot -->
<script src="<c:url value="/vendors/Flot/jquery.flot.js"/>"></script>
<script src="<c:url value="/vendors/Flot/jquery.flot.pie.js"/>"></script>
<script src="<c:url value="/vendors/Flot/jquery.flot.time.js"/>"></script>
<script src="<c:url value="/vendors/Flot/jquery.flot.stack.js"/>"></script>
<script src="<c:url value="/vendors/Flot/jquery.flot.resize.js"/>"></script>
<!-- Flot plugins -->
<script src="<c:url value="/vendors/flot.orderbars/js/jquery.flot.orderBars.js"/>"></script>
<script src="<c:url value="/vendors/flot-spline/js/jquery.flot.spline.min.js"/>"></script>
<script src="<c:url value="/vendors/flot.curvedlines/curvedLines.js"/>"></script>
<!-- DateJS -->
<script src="<c:url value="/vendors/DateJS/build/date.js"/>"></script>
<!-- JQVMap -->
<script src="<c:url value="/vendors/jqvmap/dist/jquery.vmap.js"/>"></script>
<script src="<c:url value="/vendors/jqvmap/dist/maps/jquery.vmap.world.js"/>"></script>
<script src="<c:url value="/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"/>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<c:url value="/vendors/moment/min/moment.min.js"/>"></script>
<script src="<c:url value="/vendors/bootstrap-daterangepicker/daterangepicker.js"/>"></script>
<!-- jQuery custom content scroller -->
<script src="<c:url value="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"/>"></script>
<!-- Datatables -->
<script src="<c:url value="/vendors/datatables.net/js/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-buttons/js/buttons.flash.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-buttons/js/buttons.html5.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-buttons/js/buttons.print.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"/>"></script>
<script src="<c:url value="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"/>"></script>
<script src="<c:url value="/vendors/jszip/dist/jszip.min.js"/>"></script>
<script src="<c:url value="/vendors/pdfmake/build/pdfmake.min.js"/>"></script>
<script src="<c:url value="/vendors/pdfmake/build/vfs_fonts.js"/>"></script>
<!-- Custom Theme Scripts -->
<script src="<c:url value="/build/js/custom.min.js"/>"></script>
<script src="<c:url value="/build/js/mycustom.js"/>"></script>

</body>
</html>
