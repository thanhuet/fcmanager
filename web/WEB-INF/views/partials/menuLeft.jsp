<%--
  Created by IntelliJ IDEA.
  User: sm
  Date: 7/8/17
  Time: 6:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-3 left_col  menu_fixed">


    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<c:url value="/"/>" class="site_title"><img src="/images/footballplayer1.png" style="width: 35px;height: 35px"/> <span>Football Manager</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="<c:url value="/images/img.jpg"/>" alt="." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Xin Chào,</span>
                <h2>${username}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home"></i> Home <span class="fa "></span></a>
                    </li>
                    <li><a><i class="fa fa-list"></i> Danh Sách  <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/matches"/>">Danh Sách Trận Đấu</a></li>
                            <li><a href="<c:url value="/teams"/>">Danh Sách Đội Bóng</a></li>
                            <li><a href="<c:url value="/players"/>">Danh Sách Cầu Thủ</a></li>
                            <li><a href="<c:url value="/leagues"/>">Danh Sách Giải Đấu</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Thêm  <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="form_validation.html">Thêm Vòng Đấu</a></li>
                            <li><a href="form_wizards.html">Thêm Trận Đấu</a></li>
                            <li><a href="<c:url value="/teams/add"/>">Thêm Đội Bóng</a></li>
                            <li><a href="<c:url value="/players/add"/>">Thêm Cầu Thủ</a></li>
                            <li><a href="form.html">Thêm Giải Đấu</a></li>
                            <li><a href="form_advanced.html">Thêm Mùa Giải</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-bar-chart-o"></i> Thống Kê <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<c:url value="/admin"/>">Bảng Xếp Hạng</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<c:url value="/login"/>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>