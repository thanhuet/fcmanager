CREATE TABLE teams (
  id SERIAL PRIMARY KEY  ,
  name VARCHAR(255) NOT NULL UNIQUE,
  captain_id BIGINT,
  coach VARCHAR(255)
);

CREATE TABLE leagues(
  id SERIAL PRIMARY KEY  ,
  name VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE seasons (
  id SERIAL PRIMARY KEY  ,
  name VARCHAR(100) NOT NULL ,
  league_id INT REFERENCES leagues(id)
);

CREATE TABLE rounds(
  id  SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  season_id INT REFERENCES seasons(id)
);


CREATE TABLE players (
  id SERIAL PRIMARY KEY  ,
  name VARCHAR(50) NOT NULL ,
  number_player SMALLINT ,
  age SMALLINT CHECK (age > 0 AND age < 100),
  height SMALLINT CHECK (height > 0 AND height < 200),
  weight SMALLINT CHECK (weight > 0),
  joined date,
  position VARCHAR(255) NOT NULL,
  team_id INT REFERENCES teams(id)
);

CREATE TABLE matches (
  id SERIAL PRIMARY KEY ,
  round_id INT REFERENCES rounds(id),
  home_team_id INT REFERENCES teams(id),
  away_team_id INT REFERENCES teams(id),
  start_time TIMESTAMP,
  home_team_score INT DEFAULT 0,
  away_team_score INT DEFAULT 0,
  description text,
  CHECK (home_team_id != away_team_id)
);

CREATE TABLE player_match (
  id SERIAL PRIMARY KEY ,
  match_id BIGINT NOT NULL REFERENCES matches(id),
  player_id BIGINT NOT NULL REFERENCES players(id),
  yellow_card SMALLINT,
  red_card SMALLINT,
  goal_number SMALLINT,
  own_goal_number SMALLINT
);


CREATE TABLE season_team (
  season_id INT REFERENCES seasons(id),
  team_id INT REFERENCES teams(id),
  PRIMARY KEY (season_id, team_id)
);

ALTER TABLE teams ADD FOREIGN KEY (captain_id) REFERENCES players(id);

CREATE TABLE users(
  id SERIAL PRIMARY KEY ,
  username VARCHAR(255) NOT NULL UNIQUE,
  password varchar(255) NOT NULL ,
  role varchar(50) NOT NULL,
  attempt int ,
  lastModified TIMESTAMP

);



insert into leagues values(1,'Champions League');
insert into leagues values(2,'Premier leagues');
insert into leagues values(3,'La Liga');
insert into leagues values(4,'Bundesliga');
insert into leagues values(5,'FA Cup');
insert into leagues values(6,'France Ligue');

insert into seasons values(1,'2014-2015',1);
insert into seasons values(2,'2015-2016',1);
insert into seasons values(3,'2014-2015',2);
insert into seasons values(4,'2015-2016',2);
insert into seasons values(5,'2016-2017',2);
insert into seasons values(6,'2017-2018',2);
insert into seasons values(7,'2011-2012',3);
insert into seasons values(8,'2012-2013',3);

INSERT INTO rounds VALUES (1,'round 1',1);
INSERT INTO rounds VALUES (2,'round 2',1);
INSERT INTO rounds VALUES (3,'round 3',1);
INSERT INTO rounds VALUES (4,'round 4',1);
INSERT INTO rounds VALUES (5,'round 5',1);
INSERT INTO rounds VALUES (6,'round 1',2);
INSERT INTO rounds VALUES (7,'round 2',2);
INSERT INTO rounds VALUES (8,'round 3',2);
INSERT INTO rounds VALUES (9,'round 4',2);
INSERT INTO rounds VALUES (10,'round 1',3);
INSERT INTO rounds VALUES (11,'round 2',3);

INSERT INTO users(username, password, role) values ('admin11','111','ADMIN');

INSERT INTO teams(id,name,coach) VALUES (1,'Chelsea F.C.','Antonio Conte');
INSERT INTO teams(id,name,coach) VALUES (2,'Manchester United','jose mourinho');
INSERT INTO teams(id,name,coach) VALUES (3,'Arsenal','Arsène Wenger');
INSERT INTO teams(id,name,coach) VALUES (4,'Liverpool','Jürgen Klopp');
INSERT INTO teams(id,name,coach) VALUES (5,'Real Madrid','Zinédine Zidane');

INSERT INTO season_team VALUES (1,1);
INSERT INTO season_team VALUES (1,2);
INSERT INTO season_team VALUES (1,3);
INSERT INTO season_team VALUES (1,4);
INSERT INTO season_team VALUES (1,5);
INSERT INTO season_team VALUES (2,1);
INSERT INTO season_team VALUES (2,2);
INSERT INTO season_team VALUES (2,3);
INSERT INTO season_team VALUES (3,1);
INSERT INTO season_team VALUES (3,2);
INSERT INTO season_team VALUES (3,3);
INSERT INTO season_team VALUES (3,4);
INSERT INTO season_team VALUES (4,1);
INSERT INTO season_team VALUES (4,2);
INSERT INTO season_team VALUES (4,4);
INSERT INTO season_team VALUES (4,5);
INSERT INTO season_team VALUES (5,1);
INSERT INTO season_team VALUES (5,2);
INSERT INTO season_team VALUES (5,5);
INSERT INTO season_team VALUES (6,2);
INSERT INTO season_team VALUES (6,4);
INSERT INTO season_team VALUES (7,1);
INSERT INTO season_team VALUES (7,5);
INSERT INTO season_team VALUES (8,4);
INSERT INTO season_team VALUES (8,5);

insert into players values(1,'John Terry',7,37,175,75,'1996-2-1','Defender',1);
insert into players values(2,'David Luiz',7,37,175,75,'1996-2-1','Defender',1);
insert into players values(3,'Azpilicueta',7,37,175,75,'1996-2-1','Defender',1);
insert into players values(4,'Kurt Zouma',7,37,175,75,'1996-2-1','Defender',1);
insert into players values(5,'Cesc Fabregas',7,37,175,75,'1996-2-1','Midfielde',1);
insert into players values(6,'NGolo Kanté',7,37,175,75,'1996-2-1','Midfielde',1);
insert into players values(7,'Eden Hazard',7,37,175,75,'1996-2-1','Midfielde',1);
insert into players values(8,'Victor Moses',7,37,175,75,'1996-2-1','Midfielde',1);
insert into players values(9,'Thibaut Courtois',7,37,175,75,'1996-2-1','Goalkeeper',1);
insert into players values(10,'Diego Costa',7,37,175,75,'1996-2-1','Attacking',1);
insert into players values(11,'Dominic Solanke',7,37,175,75,'1996-2-1','Attacking',1);


insert into players values(12,'David de Gea',7,37,175,75,'1996-2-1','Goalkeeper',2);
insert into players values(13,'Luke Shaw',7,37,175,75,'1996-2-1','Defender',2);
insert into players values(14,'Faustino Marcos Rojo',7,37,175,75,'1996-2-1','Defender',2);
insert into players values(15,'Chris Smalling',7,37,175,75,'1996-2-1','Defender',2);
insert into players values(16,'Axel Tuanzebe',7,37,175,75,'1996-2-1','Defender',2);
insert into players values(17,'Juan Mata Garcia',7,37,175,75,'1996-2-1','Midfielde',2);
insert into players values(18,'Paul Pogba',7,37,175,75,'1996-2-1','Midfielde',2);
insert into players values(19,'Jesse Lingard',7,37,175,75,'1996-2-1','Midfielde',2);
insert into players values(20,'Michael Carrick',7,37,175,75,'1996-2-1','Midfielde',2);
insert into players values(21,'Wayne Rooney',7,37,175,75,'1996-2-1','Attacking',2);
insert into players values(22,'Marcus Rashford',7,37,175,75,'1996-2-1','Attacking',2);

insert into players values(23,'Petr Cech',7,37,175,75,'1996-2-1','Goalkeeper',3);
insert into players values(24,'Hector Bellerin',7,37,175,75,'1996-2-1','Defender',3);
insert into players values(25,'Kieran Gibbs',7,37,175,75,'1996-2-1','Defender',3);
insert into players values(26,'Mathieu Debuchy',7,37,175,75,'1996-2-1','Defender',3);
insert into players values(27,'Gabriel Paulista',7,37,175,75,'1996-2-1','Defender',3);
insert into players values(28,'Mesut Ozil',7,37,175,75,'1996-2-1','Midfielde',3);
insert into players values(29,'Granit Xhaka',7,37,175,75,'1996-2-1','Midfielde',3);
insert into players values(30,'Santi Cazorla',7,37,175,75,'1996-2-1','Midfielde',3);
insert into players values(31,'Jeff Reine-Adelaide',7,37,175,75,'1996-2-1','Midfielde',3);
insert into players values(32,'Alexis Sanchez',7,37,175,75,'1996-2-1','Attacking',3);
insert into players values(33,'Olivier Giroud',7,37,175,75,'1996-2-1','Attacking',3);

insert into players values(34,'Simon Mignolet',7,37,175,75,'1996-2-1','Goalkeeper',4);
insert into players values(35,'Nathaniel Clyne',7,37,175,75,'1996-2-1','Defender',4);
insert into players values(36,'Joe Gomez',7,37,175,75,'1996-2-1','Defender',4);
insert into players values(37,'Joel Matip',7,37,175,75,'1996-2-1','Defender',4);
insert into players values(38,'Ragnar Klavan',7,37,175,75,'1996-2-1','Defender',4);
insert into players values(39,'Philippe Coutinho',7,37,175,75,'1996-2-1','Midfielde',4);
insert into players values(40,'Georginio Wijnaldum',7,37,175,75,'1996-2-1','Midfielde',4);
insert into players values(41,'James Milner',7,37,175,75,'1996-2-1','Midfielde',4);
insert into players values(42,'Lucas Leiva',7,37,175,75,'1996-2-1','Midfielde',4);
insert into players values(43,'Roberto Firmino',7,37,175,75,'1996-2-1','Attacking',4);
insert into players values(44,'Daniel Sturridge',7,37,175,75,'1996-2-1','Attacking',4);

insert into players values(45,'Keylor Navas',7,37,175,75,'1996-2-1','Goalkeeper',5);
insert into players values(46,'Pepe',7,37,175,75,'1996-2-1','Defender',5);
insert into players values(47,'Sergio Ramos',7,37,175,75,'1996-2-1','Defender',5);
insert into players values(48,'Marcelo Vieira',7,37,175,75,'1996-2-1','Defender',5);
insert into players values(49,'Raphael Varane',7,37,175,75,'1996-2-1','Defender',5);
insert into players values(50,'Toni Kroos',7,37,175,75,'1996-2-1','Midfielde',5);
insert into players values(51,'James Rodriguez',7,37,175,75,'1996-2-1','Midfielde',5);
insert into players values(52,'Luka Modric',7,37,175,75,'1996-2-1','Midfielde',5);
insert into players values(53,'Mateo Kovacic',7,37,175,75,'1996-2-1','Midfielde',5);
insert into players values(54,'Cristiano Ronaldo',7,37,175,75,'1996-2-1','Attacking',5);
insert into players values(55,'Karim Benzema',7,37,175,75,'1996-2-1','Attacking',5);

UPDATE teams SET captain_id=1 WHERE id=1;
UPDATE teams SET captain_id=21 WHERE id=2;
UPDATE teams SET captain_id=28 WHERE id=3;
UPDATE teams SET captain_id=36 WHERE id=4;
UPDATE teams SET captain_id=47 WHERE id=5;

INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(1,1,1,2,2,2);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(2,1,2,1,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(3,1,1,5,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(4,1,3,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(5,2,1,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(6,2,3,4,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(7,2,1,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(8,3,1,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(9,3,4,5,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(10,3,1,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(11,5,5,3,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(12,5,2,4,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(13,7,4,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(14,8,2,5,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(15,7,5,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(16,6,4,2,3,1);
INSERT INTO matches(id,round_id,home_team_id,away_team_id,home_team_score,away_team_score) VALUES(17,6,1,4,3,1);

INSERT INTO player_match VALUES(1,1,1,1,0,2,0);
INSERT INTO player_match VALUES(2,1,2,1,0,1,0);
INSERT INTO player_match VALUES(3,1,3,1,0,0,0);
INSERT INTO player_match VALUES(4,1,4,1,0,3,0);
INSERT INTO player_match VALUES(5,1,5,1,0,0,0);
INSERT INTO player_match VALUES(6,1,6,1,0,0,0);
INSERT INTO player_match VALUES(7,1,7,1,0,0,0);
INSERT INTO player_match VALUES(8,1,8,1,0,1,0);
INSERT INTO player_match VALUES(9,1,9,1,0,0,0);
INSERT INTO player_match VALUES(10,1,10,1,0,0,0);
INSERT INTO player_match VALUES(11,1,11,1,0,0,0);
INSERT INTO player_match VALUES(12,2,1,1,0,2,0);
INSERT INTO player_match VALUES(13,2,2,1,0,2,0);
INSERT INTO player_match VALUES(14,2,3,1,0,2,0);
INSERT INTO player_match VALUES(15,2,4,1,0,2,0);

DROP SEQUENCE IF EXISTS hibernate_sequence;
CREATE SEQUENCE hibernate_sequence
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.hibernate_sequence OWNER TO postgres;